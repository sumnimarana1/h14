# FOR EACH VIDEO, COUNT COMMENTS.
# SORTED DATA HAS 1 COMMENT PER LINE, ALL VIDEOIDS TOGETHER
# 
# STEP 3. REDUCE videoID and counts by summing the counts into 1 record per video

# open s.csv for reading as a file stream named s
# open r.csv for writing as a file stream named r


#this is to open the text files to read
s = open("s.csv", "r")
#this is to open the file to write the output to
r = open("r.csv", "w")
# initialize thisKey to an empty string


# initialize thisValue to 0
thisValue = 0
thisKey = ""

# output column headers
r.write('videoID,comments\n')

# for every line in s
for eachline in s:
  # use the line strip and split methods and assign to a list named data
  data = eachline.strip().split(",")

  # assign data list to named variables id and comments
  videoID, comments = data

  if videoID != thisKey:
    if thisKey:
      # output key value pair result
      r.write(thisKey + ',' + str(thisValue)+'\n')

    # start over when changing keys,initializing thisKey to id and thisValue to 0
    thisKey = videoID
    thisValue = 0

  # apply the aggregation function  
  thisValue += int(comments)

# output the final entry when done
r.write(thisKey + ',' + str(thisValue)+'\n')

# close the file streams
s.close()
r.close()
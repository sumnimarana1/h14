# FOR EACH VIDEO, COUNT COMMENTS.
# RAW DATA HAS 1 COMMENT PER LINE.
# 
# STEP 1. MAP each line, output video id and 1 for this comment

# open data.csv for reading as a file stream named i
# open m.csv for writing as a file stream named o
#this is to open the text files to read
i = open("data.csv", "r")
#this is to open the file to write the output to
o = open("m.csv", "w")


count =1
#start the for loop and goes through each line in the file
for line in i:
    #strips and splits the data at the tab-delimiter
    data = line.strip().split(",")
    count += 1
    value = 1
    # check for the bad inputans if len(data) is equal to 4 
    if len(data) == 4:
        # assign data to four named variables
        video_id,comment_text,likes,replies= data
        #ignoring the first line
        if( video_id != "id"):
            # use o.write to output the id comma 1
            o.write("{0},{1}".format(video_id,value)) # write to the file
            o.write("\n")
print(count)
#closing the file streams
i.close()
o.close()

